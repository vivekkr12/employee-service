package io.learn.employeeservice.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.learn.employeeservice.dao.EmployeeRepository;
import io.learn.employeeservice.entity.Employee;
import io.learn.employeeservice.exception.EmployeeNotFoundException;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

public class EmployeeServiceTest {
	
	@InjectMocks
	EmployeeService employeeService = new EmployeeService();
	
	@Mock
	EmployeeRepository employeeRepository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void whenEmployeeExists_thenReturned() {
		Employee mockEmployee = new Employee();
		mockEmployee.setId(1);
		mockEmployee.setFirstName("John");
		mockEmployee.setLastName("Doe");
		
		when(employeeRepository.findOne(1)).thenReturn(mockEmployee);
		
		Employee employee = employeeService.getEmployeeById(1);
		assertThat(employee).isEqualTo(mockEmployee);
	}
	
	@Test(expected = EmployeeNotFoundException.class)
	public void whenEmployeeNotExists_thenException() {
		when(employeeRepository.findOne(1)).thenReturn(null);
		employeeService.getEmployeeById(1);
	}
}
