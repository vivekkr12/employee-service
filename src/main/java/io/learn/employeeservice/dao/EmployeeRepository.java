package io.learn.employeeservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import io.learn.employeeservice.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	
}
