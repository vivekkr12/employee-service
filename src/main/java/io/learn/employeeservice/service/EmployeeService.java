package io.learn.employeeservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.learn.employeeservice.dao.EmployeeRepository;
import io.learn.employeeservice.entity.Employee;
import io.learn.employeeservice.exception.EmployeeNotFoundException;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	public Employee getEmployeeById(int id) {
		Employee emp = employeeRepository.findOne(id);
		if(emp == null) {
			throw new EmployeeNotFoundException("Employee with id " + id + " does not exist");
		}
		else
			return emp;
	}
	
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}
	
	public Employee createNewEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}
	
}
