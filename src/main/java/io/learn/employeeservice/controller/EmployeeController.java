package io.learn.employeeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.learn.employeeservice.entity.Employee;
import io.learn.employeeservice.service.EmployeeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@ApiOperation("Gets an employee for a given Id")
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Employee was fetched successfully"),
            @ApiResponse(code = 404, message = "Employee with the ID not available")
    })
	@GetMapping(value = "/employee/{id}")
	public Employee getEmployeeById(@PathVariable int id) {
		return employeeService.getEmployeeById(id);
	}
	
	@ApiOperation("Gets All the employees")
	@GetMapping(value = "/employee")
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();
	}
	
	@ApiOperation("Creates a new employee and returns it with the auto generated ID")
	@RequestMapping(value = "/employee", method = RequestMethod.POST)
	public Employee createNewEmployee(@RequestBody Employee employee) {
		return employeeService.createNewEmployee(employee);
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.PUT)
	public Employee updateEmployee(@RequestBody Employee employee) {
		return null;
	}
	
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
	public Employee deleteEmployee(@PathVariable int id) {
		return null;
	}
	
}
